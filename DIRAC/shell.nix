{ pkgs ? import ../nix/pkgs.nix }:

let

  commonPkgs = with pkgs.python3Packages; [

    matplotlib

    jupyter
    ipympl

  ];

  gpawPy = with pkgs; python3.withPackages (ps: with ps; commonPkgs );

in with pkgs; mkShell {
  buildInputs = [
    which
    git
    openssh

    qchem.gpaw
    gpawPy

  ];
}
