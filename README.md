# Configured Nix shells for working with GPAW and MEEP

You need nix installed on your machine and, ideally, have `troja` added as a cache server. Otherwise, building the required software might take a long time.
These shells are thought as playgrounds to get your feet wet with the software and not for developing large pieces of software. :smile:

The Python environments that come with the shells already include matplotlib for plotting basic plots as well as [Jupyter](https://jupyter.org/) for running jupyter notebooks.

## GPAW shell

To start a GPAW shell, you need to switch to the `gpaw/` directory and run:

```bash
$ cd gpaw  # If not already in that folder
$ nix-shell --pure  # Running as a pure shell
$ python -c "import gpaw; print(gpaw.__version__)"  # Import test
21.1.0
```

An [example test script](https://wiki.fysik.dtu.dk/gpaw/tutorials/H2/atomization.html) is provided as `GPAW.py`, that you can run (within the Nix shell) using

```bash
$ python3 GPAW.py
...
```

## MEEP shell

Similarly, for working with the MEEP program package:

```bash
$ cd meep  # If not already in that folder
$ nix-shell --pure  # Running as a pure shell
$ python -c "import meep; print(meep.__version__)"  # Import test
Using MPI version 3.1, 1 processes
1.18.0

Elapsed run time = 0.0004 s
```

An [example test script](https://meep.readthedocs.io/en/latest/Python_Tutorials/Basics/#a-straight-waveguide) is provided as `MEEP.py`, that you can run (within the Nix shell) using

```bash
$ python3 MEEP.py
...

#or, using 4 cores:
$ mpirun -np 4 python3 MEEP.py
...
```
