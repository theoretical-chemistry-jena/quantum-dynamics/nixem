{ pkgs ? import ../nix/pkgs.nix }:

let

  commonPkgs = with pkgs.python3Packages; [

    matplotlib

    jupyter
    ipympl

  ];

  meepPy = with pkgs; python3.withPackages (ps: with ps; [
    qchem.python3.pkgs.meep
  ] ++ commonPkgs );

in with pkgs; mkShell {
  buildInputs = [
    which
    git
    openssh

    meepPy
  ];
}
